<?php

use Bitrix\Highloadblock as HL;
use Bitrix\Main\Entity;
use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;

class Highload extends CBitrixComponent
{
    protected $entityCompiledClass;
    public function executeComponent()
    {
        Loc::loadMessages(__FILE__);
        $request = \Bitrix\Main\Context::getCurrent()->getRequest()->toArray();

        $this->entityCompiledClass = $this->getEntityCompiledClass();

        if (!Loader::includeModule("highloadblock")) {
            $this->returnError('HL_MODULE_NOT_INCLUDED');
        }

        if ($request['api_key'] !== $this->arParams['API_KEY']) {
            $this->returnError('BAD_API_KEY');
        }

        switch ($request['method']) {
            case "highload.add":
                $this->add($request['fields']);
                break;

            case "highload.edit":
                $this->edit($request['fields']);
                break;

            case "highload.delete":
                $this->delete($request['fields']);
                break;

            default:
                $this->returnError('BAD_METHOD');
        }
        echo \Bitrix\Main\Web\Json::encode($this->arResult);
    }

    /**
     * Возвращает текст ошибки в случае, если запрос выполнить невозможно
     * @param $code
     */
    protected function returnError($code)
    {
        $this->arResult['error'] = $code;
        $this->arResult['error_description'] = Loc::getMessage("ME_HIGHLOAD_" . $this->arResult['error']);
        echo \Bitrix\Main\Web\Json::encode($this->arResult);
        die;
    }

    /**
     * Добавляет новый элемент хайлоад-блока
     * @param $fields массив с полями name, address
     * @return bool
     */
    protected function add($fields)
    {
        // Массив полей для добавления
        $data = array(
            "UF_NAME"       => $fields['name'],
            "UF_ADDRESS"    => $fields['address'],
            "UF_CREATED_AT" => new Bitrix\Main\Type\Datetime()
        );

        $result = $this->entityCompiledClass::add($data);
        if (!$result->isSuccess()) {
            $this->arResult['success'] = false;
            $this->arResult['result'] = ['error_description' => $result->getErrorMessages()];
        } else {
            $this->arResult['success'] = true;
            $this->arResult['result'] = ['id' => $result->getId()];
        }
        return true;
    }

    /**
     * Редактирует поля элемента хайлоад-блока
     * @param $fields массив с полями name, address
     * @return bool
     */
    protected function edit($fields)
    {
        // Массив полей для редактирования
        $data = array(
            "UF_NAME"       => $fields['name'],
            "UF_ADDRESS"    => $fields['address'],
            "UF_UPDATED_AT" => new Bitrix\Main\Type\Datetime()
        );

        $result = $this->entityCompiledClass::update($fields['id'], $data);
        if (!$result->isSuccess()) {
            $this->arResult['success'] = false;
            $this->arResult['result'] = ['error_description' => $result->getErrorMessages()];
        } else {
            $this->arResult['success'] = true;
            $this->arResult['result'] = [];
        }
        return true;
    }

    /**
     * Удаляет элемент хайлоад-блока по его id
     * @param $fields массив с единственным ключом id
     * @return bool
     */
    protected function delete($fields)
    {
        $result = $this->entityCompiledClass::delete($fields['id']);
        if (!$result->isSuccess()) {
            $this->arResult['success'] = false;
            $this->arResult['result'] = ['error_description' => $result->getErrorMessages()];
        } else {
            $this->arResult['success'] = true;
            $this->arResult['result'] = [];
        }
        return true;
    }

    /**
     * Получает название класса нужного хайлоад-блока
     * @return mixed
     */
    protected function getEntityCompiledClass() {
        $hlbl = $this->arParams['HL_BLOCK_ID'];
        $hlblock = HL\HighloadBlockTable::getById($hlbl)->fetch();

        $entity = HL\HighloadBlockTable::compileEntity($hlblock);
        return $entity->getDataClass();
    }
}